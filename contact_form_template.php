<html>
<head>
	<meta charset="utf-8" /><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
</head>
<body>
	<?php
	$style = 'style="font-weight:bold; text-align:right; padding-right:10px"';
	?>
	<table style="font-family:Arial">
		<tr>
			<td <?php echo $style ?>>Nome</td>
			<td><?php echo $nome ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Email</td>
			<td><?php echo $email ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Telefone</td>
			<td><?php echo $telefone ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Área</td>
			<td><?php echo $area ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Site</td>
			<td><?php echo $site ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Objetivo</td>
			<td><?php echo $objetivo ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Newsletter</td>
			<td><?php echo $newsletter == 'on' ? 'Sim' : 'Não' ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Quantidade de Representantes</td>
			<td><?php echo $qtdRepresentantes ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Empresa tem ERP</td>
			<td><?php echo $erp ?></td>
		</tr>
		<tr>
			<td <?php echo $style ?>>Observação</td>
			<td><?php echo $observacao ?></td>
		</tr>
	</table>
</body>
</html>