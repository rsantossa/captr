var Captr = (function ($, Captr){
	Captr.contactTabs = function() {
		$('#contact-tabs').easyTabs({
			defaultContent: 1
		});
	}

	Captr.intervalBenefitsTabs = null;

	Captr.benefitsTabs = function () {
		var INTERVAL = 4000;

		$('#benefits-tabs').easyTabs({
			defaultContent: 1
		});

		Captr.intervalBenefitsTabs = setInterval(function(){
			Captr.goToBenefit(true);
		}, INTERVAL);
	}

	Captr.goToBenefit = function(goNext){
		var tabs = $('#benefits-tabs .tabs li'),
			length = tabs.length,
			activeTabIndex = tabs.filter('.active').index()

		if(goNext) {
			tabToTrigger = activeTabIndex < length - 1 ? ++activeTabIndex : activeTabIndex = 0;
		} else {
			tabToTrigger = activeTabIndex > 0 ? --activeTabIndex : activeTabIndex = length - 1;
		}

		$( tabs.get(tabToTrigger) ).find('a').trigger('click');
	}

	Captr.killIntervalBenefitsTabs = function() {
		clearInterval(Captr.intervalBenefitsTabs);
	}

	Captr.hashChange = function(){
		$(window).on('hashchange', function(e) {
			$.scrollTo(window.location.hash);
		})

		$(window).on('load', function(){
			if(window.location.hash){
				$(window).trigger('hashchange');
			}
		});
	}

	Captr.sendContactForm = function(form, form_type){
		if( $(form).valid() ){
			var submitButton = $('.submit-button', form);
			submitButton.text('Enviando...');

			$.getJSON(
				$(form).attr('action'),
				$(form).serialize() + '&form_type=' + form_type,
				function(json){
					submitButton.text(json.message)
				}
			);

			if(form_type == 'download_presentation') {
				Captr.downloadPresentation();
			}
		}
		return false;
	}

	Captr.validateContactForms = function(){
		$("#contact-tabs form").each(function(){
			$(this).validate();
		})
	}

	Captr.downloadPresentation = function() {
		window.open('ApresentacaoCaptr.pdf', '_blank');
	}

	Captr.masks = function() {
		var options = {onKeyPress: function(phone, e){   
			$target = "." + $(e.target).closest('input').attr('class');
			 
			if(/(\(11\) 9?(9|8|7)).+/i.test(phone)){
				$($target).mask('(00) 00000-0000', options);
			} 
			else {
				$($target).mask('(00) 0000-0000', options);        
			}
		}};

		$('#telefone1, #telefone2, #telefone3').mask('(00) 0000-0000', options);
	}

	Captr.init = function(){
		Captr.hashChange();
		Captr.benefitsTabs();
		Captr.contactTabs();
		Captr.masks();
		Captr.validateContactForms();
	}

	$(document).ready(Captr.init);

	return Captr;
})(jQuery, {});