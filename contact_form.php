<?php
if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows

function apply_template($tpl_file, $vars = array(), $include_globals = true) {
	extract($vars);
	if ($include_globals) extract($GLOBALS, EXTR_SKIP);
	ob_start();
	require($tpl_file);
	$applied_template = ob_get_contents();
	ob_end_clean();
	return $applied_template;
}

if($_GET['nome']) {
	require('config.php');

	$template = 'contact_form_template.php';
	$data = array('subject_prefix' => '[Captr] ');

	switch($_GET['form_type']){
		case 'download_presentation': 
			$data['subject'] = 'Baixar Apresentação';
			break;
		case 'how_much': 
			$data['subject'] = 'Quanto Custa';
			break;
		case 'contact':
			$data['subject'] = 'Contato';
			break;
		default: 
			$data['subject'] = false;
	}

	if(!$data['subject']){
		die('assunto nao identificado');
	}

	$data = array(
		'subject' => $data['subject_prefix'].stripslashes($data['subject']),
		'nome' => stripslashes(@$_GET['nome']),
		'email' => stripslashes(@$_GET['email']),
		'telefone' => stripslashes(@$_GET['telefone']),
		'area' => stripslashes(@$_GET['area']),
		'site' => stripslashes(@$_GET['site']),
		'objetivo' => stripslashes(@$_GET['objetivo']),
		'newsletter' => stripslashes(@$_GET['newsletter']),
		'qtdRepresentantes' => stripslashes(@$_GET['qtdRepresentantes']),
		'erp' => stripslashes(@$_GET['erp']),
		'observacao' => stripslashes(@$_GET['observacao'])
	);

	$body = apply_template($template, $data);

	$headers = "MIME-Version: 1.1".$quebra_linha;
	$headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
	$headers .= "From: ".$config['remetente'].$quebra_linha;
	$headers .= "Return-Path: " . $config['remetente'] . $quebra_linha;

	$sent = mail($config['destinatario'], $data['subject'], $body, $headers, "-r". $config['remetente']);

	if($sent){
		$returnData = array(
			'erro' => 0, 
			'message' => 'Email enviado!'
			);
	} else {
		$returnData = array(
			'erro' => 1, 
			'message' => 'Mensagem não enviada!'
			);
	}

	echo json_encode($returnData);
}
?>